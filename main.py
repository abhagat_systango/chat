import webapp2
from google.appengine.api import users,memcache
import controller
from gmail import Auth
import model
import httplib2
from gaesessions import get_current_session,set_current_session
import json
import pdb
import datetime
import logging 

class LoginPage(controller.Controller):
    def get(self):
        session = get_current_session()

        if session.is_active():
            self.redirect('/startpage')
        else:
            self.template_renderer('signin.html')


class Gmail(controller.Controller):
    def get(self):
        url = Auth.get_url()
        self.redirect(url)


class Redirect(controller.Controller):
    def get(self):
        session = get_current_session()
        credential = Auth.credentials(self.request.get("code"))
        email = credential.id_token['email']
        user_name = credential.id_token['name']
        session['user_email'] = email
        session['user_name'] = user_name

        if not model.Users.existing_user(email):
            model.Users.store_credential(email, user_name)
        
        self.redirect('/startpage')


class StartPage(controller.Controller):
    def get(self):
        session = get_current_session()
        if model.Users.check_user(session.get('user_email')):
            friend  = model.Users.get_users(session.get('user_email'))
            user = {"friends":friend}    
            self.template_renderer('startpage.html',user )
        else:
            self.redirect('/logout')


class Person(controller.Controller):
    def get(self,user_id):
        receiver = model.Users.get_user_email(user_id)
        # print receiver
        session = get_current_session()
        message, message_id = model.Message.retrive_message(session['user_email'], receiver)
        # print message.fetch(101)
        # pdb.set_trace()
        session['message_id'] = str(message_id)
        # print message.__dict__
        data = {'messages':message, 'message_id':session['message_id'], 'receiver_email':receiver, 'sender_email': session['user_email']}
        # print data
        self.template_renderer('chatpage.html',data)


class StoreMessage(controller.Controller):
    def post(self):
        session = get_current_session()
        receiver = self.request.get("receiver")
        model.Message.store_message(session['user_email'], receiver, 
                                        self.request.get("message"))

        

class PolledMessages(controller.Controller):
    def post(self):
        session = get_current_session()
        receiver = self.request.get("receiver")
        # print receiver +"hello"
        temp = datetime.datetime.strptime(str(session['message_id']),
                                            '%Y-%m-%d %H:%M:%S.%f')
        messages, message_id = model.Message.polled_messages( session['user_email'],
                                                                receiver, temp)
        session['message_id'] = str(message_id)
        list_msg = []

        for i in messages:
            list_msg.append(i.message)
        
        no_of_msg = len(list_msg)
        messages = list_msg
        self.response.content_type = 'application/json'
        obj = {
            'message': messages, 
            'length': no_of_msg,
            'message_id':session['message_id']
          } 
        self.response.write(json.dumps(obj))

class Logout(controller.Controller):
    def get(self):
        session = get_current_session()
        if session.is_active():
            session.terminate()
        self.redirect('/')