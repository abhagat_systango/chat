import webapp2
from oauth2client.client import OAuth2WebServerFlow
import constant

flow = OAuth2WebServerFlow(client_id = constant.CLIENT_ID,
                            client_secret = constant.CLIENT_SECRET,
                            scope = constant.SCOPE,
                            redirect_uri = constant.REDIRECT_URI)

class Auth:
    @staticmethod
    def get_url():
        authorize_url = flow.step1_get_authorize_url()
        return authorize_url

    @staticmethod
    def credentials(code='code'):
        credentials = flow.step2_exchange(code)
        return credentials

