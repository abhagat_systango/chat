import main


urls = [('/',main.LoginPage),
        ('/Gmail',main.Gmail),
        ('/Redirect',main.Redirect),
        ('/person/(.*)', main.Person),
        ('/logout', main.Logout),
        ('/startpage',main.StartPage),
        ('/storemessage',main.StoreMessage),
        ('/get_polled_messages',main.PolledMessages)
        ]